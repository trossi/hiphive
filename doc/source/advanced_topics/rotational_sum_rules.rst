.. _rotational_sum_rules:
.. highlight:: python
.. index::
   single: Rotational sum rules


Rotational sum rules
====================

In this tutorial we will study the importance of rotational sum rules and how
they can be enforced in :program:`hiphive`. These sum rules can currently only be
applied to the second order force constants.

The rotational sum rules are dependent on the translational sum rule being
fulfilled, i.e.

.. math::
    \sum_i\Phi_{ij}^{\alpha\beta} = 0.

The first rotational sum rule we consider is the Born-Huang condition

.. math::
    \sum_j\Phi_{ij}^{\alpha\beta}r_j^\gamma = \sum_j\Phi_{ij}^{\alpha\gamma}r_j^\beta,

where :math:`r_j^\beta` is the position vector of atom
:math:`j`. Furthermore we consider the Huang invariances

.. math::
    \sum_{ij}\Phi_{ij}^{\alpha\beta}r_{ij}^\gamma r_{ij}^\delta
    = \sum_{ij}\Phi_{ij}^{\gamma\delta}r_{ij}^\alpha r_{ij}^\beta.



MoS2 monolayer
--------------

We will use a MoS2 monolayer to showcase the importance of
correctly imposing rotational sum rules, which are necessary in order
to correctly obtain a quadratic dispersion for the lowermost
transverse acoustic mode in the vicinity of the :math:`\Gamma`-point.

Forces from density functional theory (DFT) calculations are supplied
in the ``MoS2_rattled_structures.extxyz`` file.


.. figure:: _static/MoS2_phonon_dispersion.svg

    Phonon dispersion for MoS2 close to the gamma point with translational (T)
    sum rules as well as Born-Huang (BH) and Huang (H) invariances enforced.

It is evident from the figure that it is important to enforce both of
the aforementioned rotational sum rules in order to obtain a correct
dispersion.


Source code
~~~~~~~~~~~

.. |br| raw:: html

   <br />

.. container:: toggle

    .. container:: header

       Set up structure container |br|
       ``examples/advanced_topics/rotational_sum_rules/MoS2_monolayer/1_setup_structure_container.py``

    .. literalinclude:: ../../../examples/advanced_topics/rotational_sum_rules/MoS2_monolayer/1_setup_structure_container.py

.. container:: toggle

    .. container:: header

       Construct force constant potentials while imposing different sum rules |br|
       ``examples/advanced_topics/rotational_sum_rules/MoS2_monolayer/2_construct_fcps.py``

    .. literalinclude:: ../../../examples/advanced_topics/rotational_sum_rules/MoS2_monolayer/2_construct_fcps.py

.. container:: toggle

    .. container:: header

       Plot the phonon dispersion for each force constant potential |br|
       ``examples/advanced_topics/rotational_sum_rules/MoS2_monolayer/3_plot_dispersions.py``

    .. literalinclude:: ../../../examples/advanced_topics/rotational_sum_rules/MoS2_monolayer/3_plot_dispersions.py


Graphene
--------

Graphene is another 2D-material where the rotational sum rules can
make a huge difference in the dispersion near the gamma point.

Functionality exists in hiphive to project existing force constants
onto a :class:`ClusterSpace <hiphive.ClusterSpace>`, as described
:ref:`here <fcs_sensing>`. Together with the functionality to enforce
the rotational sum rules this opens up for the possibility to correct
force constants calculated with external tools such as e.g., phonopoy.

Extracting the harmonic force constants with phonopy for graphene only
requires one supercell calculation. This structure together with
forces from density functional theory (DFT) calculations are supplied
here in the ``graphene_phonopy_supercell.extxyz`` file.

.. figure:: _static/graphene_phonon_dispersion.svg

    Phonon dispersion for graphene close to the gamma point.

The dispersions from phonopy and the one parametrized using the
extracted parameters closely match either other. Both dispersions
exhibit, however, a non-quadratic imaginary pocket in the vicinity of
the Gamma point. This pocket vanishes and the correct quadratic
dispersion is obtained by enforcing the rotational sum rules.


Source code
~~~~~~~~~~~
.. container:: toggle

    .. container:: header

       graphene example
       ``examples/advanced_topics/rotational_sum_rules/graphene/graphene.py``

    .. literalinclude:: ../../../examples/advanced_topics/rotational_sum_rules/graphene/1_graphene.py
